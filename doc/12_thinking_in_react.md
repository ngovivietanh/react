# 1.breack the ui into a component hierarchy 


``` javascript
class ProductCategoryRow extends React.Component {
  render() {
    const category = this.props.category;
    return (
      <tr>
        <th colSpan="2">
          {category}
        </th>
      </tr>
    );
  }
}

class ProductRow extends React.Component {
  render() {
    const product = this.props.product;
    const name = product.stocked ?
      product.name :
      <span style={{color: 'red'}}>
        {product.name}
      </span>;

    return (
      <tr>
        <td>{name}</td>
        <td>{product.price}</td>
      </tr>
    );
  }
}

class ProductTable extends React.Component {
  render() {
    const rows = [];
    let lastCategory = null;
    
    this.props.products.forEach((product) => {
      if (product.category !== lastCategory) {
        rows.push(
          <ProductCategoryRow
            category={product.category}
            key={product.category} />
        );
      }
      rows.push(
        <ProductRow
          product={product}
          key={product.name} />
      );
      lastCategory = product.category;
    });

    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class SearchBar extends React.Component {
  render() {
    return (
      <form>
        <input type="text" placeholder="Search..." />
        <p>
          <input type="checkbox" />
          {' '}
          Only show products in stock
        </p>
      </form>
    );
  }
}

class FilterableProductTable extends React.Component {
  render() {
    return (
      <div>
        <SearchBar />
        <ProductTable products={this.props.products} />
      </div>
    );
  }
}

const PRODUCTS = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];
 
ReactDOM.render(
  <FilterableProductTable products={PRODUCTS} />,
  document.getElementById('root')
);


```

* luồng chính: chia nhỏ thành các component theo cấu trúc data từ API để hiển thị 
* cấu trúc trong bài như sau: 
    * có nhiều loại sản phẩm ( category) mỗi loại sp có nhiều sản phần có (price) và (name) + (stocked)
    * sẽ hiển thị theo kiểu bảng có head là category 2 cột name, price + hightLight nếu stocked: true
* thực hiện: 
    * component to `FilterableProductTable` gọi thằng con `ProductTable` với mảng dữ liệu `PRODUCTS` 
    * mảng đó chuyển thành mảng component (view) trong table 
    * mảng gồm : 
        * ProductCategoryRow => truyền vào `PRODUCTS.category`
        * ProductRow => truyền vào `{category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},`


# Step 2: Build A Static Version in React

lời khuyên từ facebook: 
* build a static version theo data model trước => code nhiều nghĩ ít
* add event listener để thực hiện các `interactivity ` (tương tác) sau => nghĩ nhiều code ít

don’t use state at all :
* viết các component có thể reuse dùng props để chuyển dữ liệu từ cha xuống con 
* ` State is reserved only for interactivity`

xây dựng chương trình theo kiểu top-down hoặc bottom-up 

at the end of this step, you'll have a library ò reuseable components that render your data model 
`React’s one-way data flow (also called one-way binding) keeps everything modular and fast.`

## Props vs State
 props: properties => thuốc tính lưu các thuộc tính 
 state lưu các trạng thái phương thức
 
* props thì chuyển dữ liệu từ cha xuống các con để hiển thị quản lý dữ liệu 
* state thì chuyển dữ liệu từ con lên cha để quản lí state của các components

# Step 3: Identify The Minimal (but complete) Representation Of UI State
 
tìm ra số lượng state nhỏ nhất để đại diện có các trạng thái dữ liệu ( dùng chung )


`
1. Is it passed in from a parent via props? If so, it probably isn’t state.
2. Does it remain unchanged over time? If so, it probably isn’t state.
3. Can you compute it based on any other state or props in your component? If so, it isn’t state.

`

# Step 4: Identify Where Your State Should Live





